#include "stdafx.h"
#include "iostream"
#include <string>
#include <ctime>
#include <stack>


using namespace std;

template <typename pizza>
class stos {
private:
	int rozmiar;
	pizza *T;
	int koniec;

public:
	stos(int p);
	void dodaj(pizza liczba);
	void usun_wszystko();
	void usun();
	void wyswietl();
	pizza last();
	int size();
};

template<typename pizza>
stos<pizza>::stos(int p)
{
	rozmiar = p;
	T = new pizza[rozmiar];
	koniec = -1;
}

template<typename pizza>
pizza stos<pizza>::last()
{
	return T[rozmiar - 1];
}

template<typename pizza>
int stos<pizza>::size()
{
	return rozmiar;
}

template<typename pizza>
void stos<pizza>::usun() {
	if (koniec == (-1))
	{
		cout << "stos jest pusty" << endl;
	}
	else
	{
		koniec = koniec - 1;
	}
}

template<typename pizza>
void stos<pizza>::usun_wszystko() {
	while (koniec != -1)
	{
		koniec = -1;
	}
}

template<typename pizza>
void stos<pizza>::dodaj(pizza liczba) {
	if (koniec == rozmiar - 1)
	{
		pizza *H = new pizza[rozmiar];
		for (int i = 0; i < rozmiar; i++)
		{
			H[i] = T[i];
		}
		delete T;
		int cos = rozmiar;
		rozmiar = 2 * rozmiar;
		T = new pizza[rozmiar];
		for (int i = 0; i < cos; i++)
		{
			T[i] = H[i];
		}
		delete H;
	}
	koniec = koniec + 1;
	T[koniec] = liczba;
}

template<typename pizza>
void stos<pizza>::wyswietl() {
	int pomoc;
	if (koniec > -1)
	{
		pomoc = koniec;

		while (pomoc != -1)
		{
			cout << T[pomoc] << endl;
			pomoc = pomoc - 1;
		}
	}
	else
		cout << "Stos jest pusty." << endl;
}

int main()
{
	int ilosc = 500000;
	clock_t t1, t2 = 0;
	stos<int> *T = new stos<int>(5);
	stack<int> S;
	
	t1 = clock();
	for (int i = 0; i < ilosc; i++)
	{
		S.push(rand() % 10);
		//cout << S.top() << endl;
	}
	t2 = clock();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";

	t1 = clock();
	for (int i = 0; i < ilosc; i++)
	{
		T->dodaj(rand() % 10);
	}
	t2 = clock();
	//T->wyswietl();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	return 0;
}