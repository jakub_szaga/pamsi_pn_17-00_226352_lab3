#include "stdafx.h"
#include "iostream"
#include <string>
#include <ctime>
#include <stdlib.h>
using namespace std;

int kroki = 0;

template <typename pizza>
class stos {
private:
	int rozmiar;
	pizza *T;
	int koniec;
	string adres;

public:
	stos(int p, string numer);
	void dodaj(pizza liczba);
	void usun_wszystko();
	void usun();
	void wyswietl();
	pizza last();
	int size();
};

template<typename pizza>
stos<pizza>::stos(int p, string numer)
{
	if (p == 0)
		p = 1;
	rozmiar = p;
	T = new pizza[rozmiar];
	koniec = -1;
	adres = numer;
}

template<typename pizza>
pizza stos<pizza>::last()
{
	return T[koniec];
}

template<typename pizza>
int stos<pizza>::size()
{
	return rozmiar;
}

template<typename pizza>
void stos<pizza>::usun() {
	if (koniec == (-1))
	{
		cout << "stos jest pusty" << endl;
	}
	else
	{
		koniec = koniec - 1;
	}
}

template<typename pizza>
void stos<pizza>::usun_wszystko() {
	while (koniec != -1)
	{
		koniec = -1;
	}
}

template<typename pizza>
void stos<pizza>::dodaj(pizza liczba) {
	if (koniec == rozmiar - 1)
	{
		pizza *H = new pizza[rozmiar];
		for (int i = 0; i < rozmiar; i++)
		{
			H[i] = T[i];
		}
		delete T;
		int cos = rozmiar;
		rozmiar = 2 * rozmiar;
		T = new pizza[rozmiar];
		for (int i = 0; i < cos; i++)
		{
			T[i] = H[i];
		}
		delete H;
	}
	koniec = koniec + 1;
	T[koniec] = liczba;
}

template<typename pizza>
void stos<pizza>::wyswietl() {
	int pomoc;
	cout << "Stos " << adres <<" : "<< endl;
	if (koniec > -1)
	{
		pomoc = koniec;
		while (pomoc != -1)
		{
			cout << T[pomoc] << endl;
			pomoc = pomoc - 1;
		}
	}
	else
		cout << "Stos jest pusty." << endl;
}

template<typename pizza>
void Hanoi(int ilosc, stos<pizza> *z, stos<pizza> *przez, stos<pizza> *na, int krok)
{
	
		if (ilosc > 0)
		{
			//kroki++;
			//cout << "wchodze" << endl;
			Hanoi(ilosc - 1, z, na, przez, krok);
			if (kroki == krok) {
				cout << "Wyjscie: " << endl;
				//cout << "Stos ";//<< stosy(z);
				//cout<< endl;
				z->wyswietl();
				//cout << "Stos buforowy:" << endl;
				przez->wyswietl();
				//cout << "Stos koncowy:" << endl;
				na->wyswietl();
				system("PAUSE");
			}
			kroki++;
			int cos = z->last();
			na->dodaj(cos);
			z->usun();
			Hanoi(ilosc - 1, przez, z, na, krok);
	}
}

int main()
{
	int krok;
	int ilosc;
	stos<int> *z = new stos<int>(0,"poczatkowy");
	stos<int> *przez = new stos<int>(0,"bufor");
	stos<int> *na = new stos<int>(0,"koncowy");
	cout << "Podaj rozmiar wiezy: ";
	cin >> ilosc;
	for (int i = ilosc; i > 0; i--)
		z->dodaj(i);
	cout << endl;
	cout << "Podaj krok na ktorym chcesz sie zatrzymac: ";
	cin >> krok;
	cout << endl;
	//z->wyswietl();
	Hanoi(ilosc, z, przez, na, krok);
	//z->wyswietl();
	cout << "Wyjscie: " << endl;
	//cout << "Stos poczatkowy:" << endl;
	z->wyswietl();
	//cout << "Stos buforowy:" << endl;
	przez->wyswietl();
	//cout << "Stos koncowy:" << endl;
	na->wyswietl();
	return 0;
}