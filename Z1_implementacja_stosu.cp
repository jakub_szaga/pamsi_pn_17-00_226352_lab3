
#include "stdafx.h"
#include "iostream"
#include <string>
#include <ctime>
using namespace std;

template <typename pizza>
class stos {
private:
	int rozmiar;
	pizza *T;
	int koniec;

public:
	stos(int p);
	void dodaj(pizza liczba);
	void usun_wszystko();
	void usun();
	void wyswietl();
	pizza last();
	int size();
};

template<typename pizza>
stos<pizza>::stos(int p)
{
	rozmiar = p;
	T = new pizza[rozmiar];
	koniec = -1;
}

template<typename pizza>
pizza stos<pizza>::last()
{
	return T[rozmiar - 1];
}

template<typename pizza>
int stos<pizza>::size()
{
	return rozmiar;
}

template<typename pizza>
void stos<pizza>::usun() {
	if (koniec == (-1))
	{
		cout << "stos jest pusty" << endl;
	}
	else
	{
		koniec = koniec-1;
	}
}

template<typename pizza>
void stos<pizza>::usun_wszystko() {
	while (koniec != -1)
	{
		koniec = -1;
	}
}

template<typename pizza> 
void stos<pizza>::dodaj(pizza liczba) {
	if (koniec == rozmiar - 1)
	{
		pizza *H = new pizza[rozmiar];
		for (int i = 0; i < rozmiar; i++)
		{
			H[i] = T[i];
		}
		delete T;
		int cos = rozmiar;
		rozmiar = 2*rozmiar;
		T = new pizza[rozmiar];
		for (int i = 0; i < cos; i++)
		{
			T[i] = H[i];
		}
		delete H;
	}
	koniec = koniec + 1;
	T[koniec] = liczba;
}

template<typename pizza>
void stos<pizza>::wyswietl() {
	int pomoc;
	if (koniec > -1)
	{
		pomoc = koniec;
		
		while (pomoc != -1)
		{
			cout << T[pomoc]<< endl;
			pomoc = pomoc - 1;
		}
	}
	else
		cout << "Stos jest pusty." << endl;
}

int main()
{
	stos<int> *T= new stos<int>(0);
	int wybor = 0;

	int cos;
	do {
		cout << "Witaj!" << endl;
		cout << "1.Wypelnij liste znakami." << endl;
		cout << "2.Usun wybrane elementy listy." << endl;
		cout << "3.Wyswietl liste." << endl;
		cout << "4.Oproznij liste." << endl;
		cout << "5.Zakoncz." << endl;
		cin >> wybor;
		switch (wybor) {
		case 1:
		{
			cout << "Podaj liczbe:" << endl;
			cin >> cos;
				T->dodaj(cos);
		}
		break;
		case 2:
		{
			T->usun();
		}
		break;
		case 3:
		{
			cout << "Oto lista:" << endl;
			T->wyswietl();
		}
		break;
		case 4:
		{
			T->usun_wszystko();
		}
		break;
		case 5:
		{
			cout << "Koniec" << endl;
		}
		break;
		default:
		{
			cout << "Brak takiej opcji." << endl;
		}
		}
	} while (wybor != 5);

	//cout << list->begin->liczba << endl;
	//cout << list->begin->nast->liczba << endl;

	//list->wyswietl(proba);
	delete(T);
	return 0;
}